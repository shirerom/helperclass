<?php

require '../vendor/autoload.php';
require '../src/H.php';



class HTest  extends PHPUnit_Framework_TestCase {
	
	public function test_weeksInMonth() {

		$this->assertEquals(4, H::weeksInMonth(2010, 2));
		$this->assertEquals(5, H::weeksInMonth(2010, 3));
		$this->assertEquals(6, H::weeksInMonth(2010, 8));
		$this->assertEquals(5, H::weeksInMonth(2012, 2));
	}
	
	public function test_offsetInMonth() {

		$this->assertEquals(6, H::offsetInMonth(2012, 1));
		$this->assertEquals(5, H::offsetInMonth(2012, 9));
		$this->assertEquals(4, H::offsetInMonth(2012, 6));
		$this->assertEquals(3, H::offsetInMonth(2012, 11));
		$this->assertEquals(2, H::offsetInMonth(2012, 8));
		$this->assertEquals(1, H::offsetInMonth(2012, 5));
		$this->assertEquals(0, H::offsetInMonth(2012, 10));
	}
	
}
