<?php

/*
 * Exceptions
 */
class HExc extends Exception {}
class HUnknownDateFormatExc extends HExc {}
class HUnknownDateSeparatorExc extends HExc {}



/**
 * Klasse mit Hilfsfunktionen
 */
class H {

	const SECONDS_PER_DAY = 86400;
	
	const SECONDS_PER_YEAR = 31536000;
	
	const MINUTES_PER_HOUR = 60;

	/**
	 * konvertiert einen CSV-String in ein Array, weil str_getcsv() so scheiße ist
	 * 
	 * @param string $csv 
	 * @param bool $cutFirstLine
	 * @param string $delimiter
	 * @param bool $removeQuotes
	 * 
	 * @return array
	 */
	public static function csv2array(&$csv, $cutFirstLine = false, $delimiter = ",", $removeQuotes = false) {

		$lines = \preg_split("/\n/", $csv);

		$array = array();

		foreach($lines as &$line) {
			$array[] = \preg_split("/{$delimiter}/", $line);
		}

		if($cutFirstLine === true) {
			array_shift($array);
		}
		
		if ($removeQuotes) {
			
			for ($i = 0; $i < \count($array); ++$i) {
				
				for ($j = 0; $j < \count($array[$i]); ++$j) {
					
					$array[$i][$j] = preg_replace("/\"(.+)\"/", "$1", $array[$i][$j]);
				}
			}
		}

		return $array;
	}
	

	/**
	 * generiert einen Timestamp aus einem Datum
	 * 
	 * folgende Formate werden erkannt:
	 *	- dd.mm.yyyy
	 *	- yyyy-mm-dd
	 *	- mm/dd/yyyy
	 *	- yyyy-mm-ddThh:mm:ssZ
	 * 
	 * @param string $dateString Datum
	 * 
	 * @return int Timestamp 
	 * 
	 * @throws HUnknownDateFormatExc
	 */
	public static function date2ts($dateString) {

		$hour = 0;
		$minute = 0;
		$second = 0;
		$month = 0;
		$day = 0;
		$year = 0;

		// dd.mm.yyyy
		if(preg_match("/^\d{2}\.\d{2}\.\d{4}$/", $dateString)) {
			list($day, $month, $year) = preg_split("/\./", $dateString);
		}

		// dd.mm.yy
		elseif(preg_match("/^\d{2}\.\d{2}\.\d{2}$/", $dateString)) {
			list($day, $month, $year) = preg_split("/\./", $dateString);
		}

		// yyyy-mm-dd
		elseif(preg_match("/^\d{4}-\d{2}-\d{2}$/", $dateString)) {
			list($year, $month, $day) = preg_split("/-/", $dateString);
		}
		
		// mm/dd/yyyy
		elseif(preg_match("/^\d{2}\/\d{2}\/\d{4}$/", $dateString)) {
			list($month, $day, $year) = preg_split("/\//", $dateString);
		}
		
		// yyyy-mm-ddThh:mm:ssZ
		elseif(preg_match("/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}Z$/", $dateString)) {
			
			$dateString = substr($dateString, 0, strlen($dateString) - 1);
			
			list($date, $time) = preg_split("/T/", $dateString);
			list($year, $month, $day) = preg_split("/-/", $date);
			list($hour, $minute, $second) = preg_split("/:/", $time);
		}
		
		// yyyy-mm-ddThh:mm
		elseif(preg_match("/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}$/", $dateString)) {
			
			list($date, $time) = preg_split("/T/", $dateString);
			list($year, $month, $day) = preg_split("/-/", $date);
			list($hour, $minute) = preg_split("/:/", $time);
		}
		
		else {
			throw new HUnknownDateFormatExc($dateString);
		}
		
		return mktime((int)$hour, (int)$minute, (int)$second, (int)$month, (int)$day, (int)$year);
	}

	public static function offsetInMonth($year, $month) {
		
		$weekDay = date("N", mktime(0, 0, 0, $month, 1, $year)); // monday == 1, sunday == 7
		
		return $weekDay - 1;
	}
	
	public static function weeksInMonth($year, $month) {
	
		$daysInMonth = cal_days_in_month(CAL_GREGORIAN, $month, $year);
		$weekDay = date("N", mktime(0, 0, 0, $month, 1, $year)); // monday == 1, sunday == 7

		// February
		if ($daysInMonth == 28) {			
			return ($weekDay == 1) ? 4 : 5;
		}
		
		else if ($daysInMonth == 29) {
			return 5;
		}
		
		else if ($daysInMonth == 30) {
			return ($weekDay < 7) ? 5 : 6;
		}
		
		else if ($daysInMonth == 31) {
			return ($weekDay < 6) ? 5 : 6;
		}
	}
	
	public static function addTime(array $time1, array $time2) {
	
		$resultTime = [];
		
		$minutes = $time1[1] + $time2[1];
		
		$resultTime[1] = ($minutes < self::MINUTES_PER_HOUR) ? $minutes : $minutes % self::MINUTES_PER_HOUR;
		$resultTime[0] = intval($minutes / self::MINUTES_PER_HOUR);
		
		$resultTime[0] += $time1[0] + $time2[0];
		
		return $resultTime;
	}
	
	/**
	 * mktime() ohne Uhrzeit
	 * 
	 * @param int $day
	 * @param int $month
	 * @param int $year
	 * @return int 
	 */
	public static function ts($day = NULL, $month = NULL, $year = NULL) {

		if (is_null($day)) {
			$day = date("j");
		}
		
		if (is_null($month)) {
			$month = date("n");
		}
		
		if (is_null($year)) {
			$year = date("Y");
		}

		return mktime(0, 0, 0, (int)$month, (int)$day, (int)$year);
	}

	
	/**
	 * generiert eine formatiertes Datum aus einem Timestamp
	 *
	 * folgende Formate können, abhängig vom Separator, generiert werden:
	 *	- dd.mm.yyyy
	 *	- yyyy-mm-dd
	 *	- mm/dd/yyyy
	 * 
	 * @param int $timestamp
	 * @param string $separator
	 *
	 * @return string formatiertes Datum
	 * 
	 * @throws HUnknownDateSeparatorExc
	 */
	public static function ts2date($timestamp, $separator = ".") {

		// dd.mm.yyyy
		if($separator == ".") {
			return date("d.m.Y", $timestamp);
		}

		// yyyy-mm-dd
		elseif($separator == "-") {
			return date("Y-m-d", $timestamp);
		}

		// mm/dd/yyyy
		elseif($separator == "/") {
			return date("m/d/Y", $timestamp);
		}

		else {
			throw new HUnknownDateSeparatorExc($separator);
		}
	}

	
	/**
	 * generiert eine formatiertes Datum und Zeit aus einem Timestamp
	 *
	 * folgende Formate können, abhängig vom Separator, generiert werden:
	 *	- dd.mm.yyyy hh:mm
	 *	- yyyy-mm-dd hh:mm
	 *	- mm/dd/yyyy hh:mm
	 * 
	 * @param int $timestamp
	 * @param string $separator
	 *
	 * @return string formatiertes Datum
	 * 
	 * @throws HUnknownDateSeparatorExc
	 */
	public static function ts2dateTime($timestamp, $separator = ".") {

		// dd.mm.yyyy
		if($separator == ".") {
			return date("d.m.Y H:i", $timestamp);
		}

		// yyyy-mm-dd
		elseif($separator == "-") {
			return date("Y-m-d H:i", $timestamp);
		}

		// mm/dd/yyyy
		elseif($separator == "/") {
			return date("Y-m-d H:i", $timestamp);
		}

		else {
			throw new HUnknownDateSeparatorExc($separator);
		}
	}

	
	/**
	 * generiert eine Array mit Jahr, Monat und Tag aus einem Timestamp
	 *
	 * @param int $timestamp
	 *
	 * @return array [Jahr, Monat, Tag]
	 */
	public static function ts2dateArray($timestamp) {

		return [
			date("Y", $timestamp),
			date("m", $timestamp),
			date("d", $timestamp)
		];
	}

	
	/**
	 * Timestamp aus EXIF-DateTime
	 * 
	 * EXIF-DateTime-Format: YYYY:MM:DD hh:mm:ss
	 * 
	 * @param string $exifTimeDate
	 * @return int
	 */
	public static function exifDateTime2ts($exifTimeDate) {
		
		list($date, $time) = preg_split("/ /", $exifTimeDate);
		
		list($year, $month, $day) = preg_split("/:/", $date);
		list($hour, $minute, $second) = preg_split("/:/", $time);
		
		return mktime($hour, $minute, $second, $month, $day, $year);
	}

	/**
	 * flacht ein Array ab auf eine Dimension
	 * 
	 * @param array $array
	 * @return array
	 */
	public static function flatArray(&$array) {

		$arrayFlattened = array();
    
    $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($array));

    foreach($it as $v) {
      $arrayFlattened[] = $v;
    }

//		array_walk_recursive($array, create_function('$val, $key, $obj', 'array_push($obj, $val);'), $arrayFlattened);

		return $arrayFlattened;
	}

	/**
	 *
	 * @param <type> $value
	 * @return string
	 */
	public static function point2komata($value) {
		return preg_replace("/\./", ",", (string)$value);
	}

	/**
	 *
	 * @param <type> $value
	 * @return string
	 */
	public static function komata2point($value) {
		return preg_replace("/,/", ".", (string)$value);
	}
	
	/**
	 * 
	 * @param scalar $variable
	 * @return bool
	 */
	public static function isFloat($variable) {
		return (filter_var($variable, FILTER_VALIDATE_FLOAT) !== FALSE);
	}
	
	/**
	 * 
	 * @param scalar $variable
	 * @return bool
	 */
	public static function isInt($variable) {
		return (filter_var($variable, FILTER_VALIDATE_INT) !== FALSE);
	}
	
	/**
	 * 
	 * @param string $location
	 */
	public static function redirect($location) {
		header("Location: ".filter_var($location, FILTER_SANITIZE_URL));
		exit;
	}
	
	/**
	 * converts a json string to an array
	 * 
	 * @param string $json
	 * @return array
	 */
	public static function json2array($json) {
		return json_decode($json, TRUE);
	}
	
	/**
	 * converts an array to a json string
	 * 
	 * @param array $array
	 * @return string json
	 */
	public static function array2json($array) {
		return json_encode($array);
	}
	
	/**
	 * reads json from a given file and converts its content to an array
	 * 
	 * @param string $file
	 * @return array
	 */
	public static function arrayFromJsonFile($file) {
		return self::json2array(file_get_contents($file));
	}
}


